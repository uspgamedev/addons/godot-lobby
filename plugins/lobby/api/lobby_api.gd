extends Node
class_name LobbyAPI

@export var config: LobbyConfig = LobbyConfig.new()
@export var is_server := false

@onready var client: LobbyClient = $Client
@onready var server: LobbyServer = $Server

signal room_updated(room: LobbyRoom)
signal game_started(room: LobbyRoom)

# ------------------------------------------------------------------
# |                              API                               |
# ------------------------------------------------------------------

func list_rooms() -> Array[LobbyRoom]:
	var list := [] as Array[LobbyRoom]
	var dictionary_list := await server.list_rooms(config)
	dictionary_list.map(func(x): list.push_back(LobbyRoom.new(x.room_name, x.player_infos)))
	return list

func enter_room(room_name: String, player_info: Dictionary):
	client.enter_room(room_name, player_info, config)

func change_info(player_info: Dictionary):
	client.change_info(player_info)

func leave_room():
	client.leave_room()

func set_ready(value: bool):
	client.set_ready(value)

func disconnect_from_server():
	await server.disconnect_from_server()

# ------------------------------------------------------------------
# |                            PRIVATE                             |
# ------------------------------------------------------------------

func _ready():
	if is_server:
		get_tree().root.mode = Window.MODE_MINIMIZED
	
		if OS.get_environment("SERVER_PORT") != "":
			config.server_port = int(OS.get_environment("SERVER_PORT"))
		
		var disable_dtls_env_var := OS.get_environment("DISABLE_DTLS")
		config.use_dtls = config.use_dtls if disable_dtls_env_var == "" else (disable_dtls_env_var != "true")
		config.validate_certificate = OS.get_environment("VALIDATE_CERT") != "" 
		
		if OS.get_environment("KEY_PATH") != "":
			config.server_private_key_file = "res://" + OS.get_environment("KEY_PATH")
		if OS.get_environment("CERT_PATH") != "":
			config.certificate_file = "res://" + OS.get_environment("CERT_PATH")
			
		var protocol := OS.get_environment("PROTOCOL")
		var protocol_id := LobbyConfig.Protocol.keys().find(protocol)
		if protocol_id != -1:
			config.server_protocols = [protocol_id as LobbyConfig.Protocol]
	
	server.init_server(is_server, client, config)
	client.init_client(is_server, server)

func _on_client_room_updated(room):
	room_updated.emit(room)

func _on_client_game_started(room):
	game_started.emit(room)

extends Node
class_name LobbyServer
const MAX_USERS = 100

var is_authority := false

@onready var client: LobbyClient

var server_socket : MultiplayerPeer

signal client_connected(client_id: int)
signal client_disconnected(client_id: int)

signal rooms_listed(list: Array[Dictionary])

func _ready():
	multiplayer.peer_connected.connect(func(x): client_connected.emit(x))
	multiplayer.peer_disconnected.connect(func(x): client_disconnected.emit(x))
	multiplayer.peer_disconnected.connect(remove_peer)
	multiplayer.peer_connected.connect(func(x): print("Connected: ", x))
	multiplayer.peer_disconnected.connect(func(x): print("Disconnected: ", x))

func init_server(is_authority_: bool, client_: LobbyClient, config: LobbyConfig):
	is_authority = is_authority_
	client = client_
	
	if is_authority:
		listen(config)

var active := false
var cached_config

func listen(config: LobbyConfig): 
	cached_config = config
	
	if config.server_protocols.size() == 0:
		push_error("No protocol selected")
		return
		
	if config.server_protocols.size() > 1:
		push_error("Multiple protocols not yet supported")
		return
	
	if config.server_protocols.find(LobbyConfig.Protocol.ENET) != -1:
		server_socket = ENetMultiplayerPeer.new()
		
		if server_socket.create_server(config.server_port, MAX_USERS) != OK:
			push_error("Cannot create server")
			return
		
		if config.use_dtls:
			var tls_options := get_tls_options(config)

			server_socket.host.dtls_server_setup(tls_options)
			
	elif config.server_protocols.find(LobbyConfig.Protocol.WEB_SOCKET) != -1:
		server_socket = WebSocketMultiplayerPeer.new()
	
		if server_socket.create_server(config.server_port, "*", get_tls_options(config)) != OK:
			push_error("Cannot create server")
			return
	else:
		push_error("Protocol not supported")
		return
		
	multiplayer.set_multiplayer_peer(server_socket)
	active = true

func remove_peer(id: int):
	if is_authority:
		for room_name in rooms.keys():
			if rooms[room_name].player_infos.has(id):
				leave_room(room_name, id)
				server_socket.disconnect_peer(id)

func _process(_delta):
	if is_authority and active and \
		multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		listen(cached_config)
	
	if not is_instance_valid(multiplayer.multiplayer_peer) or multiplayer.multiplayer_peer.get_connection_status() != MultiplayerPeer.CONNECTION_CONNECTED:
		rooms.clear()

func get_tls_options(config: LobbyConfig) -> TLSOptions:
	if not config.use_dtls:
		return null
		
	if not ResourceLoader.exists(config.server_private_key_file) or not ResourceLoader.exists(config.certificate_file):
		push_error("Key or Certificate does not exist")
		get_tree().quit()
		return null
	
	var key = load(config.server_private_key_file)
	var cert = load(config.certificate_file)
	
	return TLSOptions.server(key, cert)

var _list: Array[Dictionary] = []

func list_rooms(config: LobbyConfig) -> Array[Dictionary]:
	if is_authority:
		push_error("Invalid Rpc request")
		return [] as Array[Dictionary]
	
	if not client.is_connected_to_server():
		client.try_connect(config, config.connection_tries)
		if await client.await_connection_status() != LobbyClient.ConnectionStatus.CONNECTED:
			push_error("Cannot connect to server")
			return []
		
	list_rooms_rpc.rpc_id(1)
	rooms_listed.connect(func(x): self._list = x, CONNECT_ONE_SHOT)
	await rooms_listed
	return _list

func disconnect_from_server():
	if is_instance_valid(multiplayer.multiplayer_peer):
		multiplayer.multiplayer_peer.close()
		multiplayer.multiplayer_peer = null
	

# ------------------------------------------------------------------
# |                           SERVER  RPCS                         |
# ------------------------------------------------------------------

var rooms := {} # String -> LobbyRoom

@rpc("any_peer", "call_remote", "reliable")
func join_room(room_name: String, player_info: Dictionary):
	var room: LobbyRoom
	
	if not rooms.has(room_name):
		room = LobbyRoom.new(room_name)
		rooms[room_name] = room
		room.room_seed = randi()
	else:
		room = rooms[room_name]
	
	player_info.id = multiplayer.get_remote_sender_id()
	
	if room.has_player_id(player_info.id):
		# TODO: Send error to client
		return
		
	room.add_player_info(player_info)
	
	for info in room.player_infos.values():
		client.update_room.rpc_id(info.id, room_name, room.player_infos)

@rpc("any_peer", "call_remote", "reliable")
func player_ready(room_name: String, is_ready := true):
	var sender_id = multiplayer.get_remote_sender_id()
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if room == null or not room.player_infos.has(sender_id):
		# TODO: Send error to client
		return
		
	room.player_infos[sender_id].ready = is_ready
	change_info(room_name, room.player_infos[sender_id])
	
@rpc("any_peer", "call_remote", "reliable")
func change_info(room_name: String, player_info: Dictionary):
	var sender_id = multiplayer.get_remote_sender_id()
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if room == null or not room.player_infos.has(sender_id):
		# TODO: Send error to client
		return

	player_info.id = sender_id
	if not player_info.has("ready"):
		player_info.ready = room.player_infos[sender_id].ready
	room.player_infos[sender_id] = player_info
	
	for peer_id in room.player_infos.keys():
		if multiplayer.get_peers().find(peer_id) == -1:
			room.player_infos.erase(peer_id)
	
	for info in room.player_infos.values():
		client.update_room.rpc_id(info.id, room_name, room.player_infos)
	
	if room.player_infos.values().filter(func(x): return not x.ready).size() > 0:
		return
		
	for info in room.player_infos.values():
		client.start_game.rpc_id(info.id, room.player_infos, room.room_seed)
	
	rooms.erase(room_name)

#@rpc("any_peer", "call_remote", "reliable")
func leave_room(room_name: String, id = null):
	var room: LobbyRoom = rooms.get(room_name, null)
	
	if multiplayer.get_remote_sender_id() == 1:
		id = multiplayer.get_remote_sender_id()
	
	if not rooms.has(room_name) or not room.player_infos.has(id):
		# TODO: Send error to client
		return
		
	if not room.has_player_id(id):
		# TODO: handle error
		return
		
	room.remove_player_info(id)
	
	for key in room.player_infos.keys():
		room.player_infos[key].ready = false
	
		
	if room.get_num_players() == 0:
		rooms.erase(room_name)
	else:
		for info in room.player_infos.values():
			client.update_room.rpc_id(info.id, room_name, room.player_infos)

@rpc("any_peer", "call_remote", "reliable")
func list_rooms_rpc():
	if not is_authority:
		push_error("Invalid Rpc request")
		return []
		
	var list = [] as Array[Dictionary]
	rooms.values().filter(func(x: LobbyRoom): return x.get_num_players() > 0)\
		.map(func(x: LobbyRoom): list.push_back(x.serialize()))
		
	return_room_list_rpc.rpc_id(multiplayer.get_remote_sender_id(), list)
	
@rpc("authority", "call_remote", "reliable")
func return_room_list_rpc(list: Array[Dictionary]):
	rooms_listed.emit(list)

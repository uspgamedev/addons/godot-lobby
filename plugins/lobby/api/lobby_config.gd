extends Resource
class_name LobbyConfig

enum Protocol {
	ENET,
	WEB_SOCKET
}

@export_group("Client Only")
@export var server_address := "127.0.0.1:8080" 
@export var client_protocol : Protocol = Protocol.ENET
@export var connection_tries := 10
## In seconds
@export var time_between_tries := 0.5

@export_group("Server Only")
@export var server_port := 8080
@export var server_protocols : Array[Protocol] = [Protocol.ENET]


@export_group("Security")
@export var use_dtls := true

@export_subgroup("Client Only")
@export_file var certificate_file = "res://plugins/lobby/example/cert.crt"
@export var validate_certificate := true

@export_subgroup("Server Only")
@export_file var server_private_key_file = "res://plugins/lobby/example/key.key"

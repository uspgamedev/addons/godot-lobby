extends Node
class_name LobbyClient

var client_socket: MultiplayerPeer

var is_server := false
@onready var server: LobbyServer

var current_player_info := {}
var room: LobbyRoom = null

enum Connection {
	DISCONNECTED,
	CONNECTING,
	CONNECTED,
	
}

var active := Connection.DISCONNECTED

enum ConnectionStatus {
	DISCONNECTED,
	CONNECTED,
	CONNECTING,
	TIMEOUT
}

signal connection_status(status: ConnectionStatus)

# ------------------------------------------------------------------
# |                              API                               |
# ------------------------------------------------------------------

func enter_room(room_name: String, player_info: Dictionary, config: LobbyConfig):
	room = LobbyRoom.new(room_name)
	current_player_info = player_info
	if not is_connected_to_server():
		try_connect(config, config.connection_tries)
		if await await_connection_status() != ConnectionStatus.CONNECTED:
			push_error("Cannot connect to server")
			return
	entered_room()
	

func try_connect(config: LobbyConfig, tries: int = 0):
	if not is_instance_valid(multiplayer.multiplayer_peer) or multiplayer.multiplayer_peer is OfflineMultiplayerPeer or \
		 multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		connect_to_server(config)
	
	if not is_connected_to_server() and tries > 0:
		get_tree().create_timer(config.time_between_tries).timeout.connect(try_connect.bind(config, tries-1), CONNECT_ONE_SHOT)
	elif not is_connected_to_server() and tries > 0:
		connection_status.emit(ConnectionStatus.TIMEOUT)
	
var _current_connection_status := ConnectionStatus.DISCONNECTED

func await_connection_status() -> ConnectionStatus:
	connection_status.connect(func(status: ConnectionStatus):
		self._current_connection_status = status
		, CONNECT_ONE_SHOT)
	await connection_status
	return _current_connection_status
	
func change_info(player_info: Dictionary):
	if room == null:
		return
	
	server.change_info.rpc_id(1, room.room_name, player_info)

func leave_room():
	if room != null:
		client_socket.close()
		room = null

func set_ready(value: bool):
	if room == null:
		return
	
	server.player_ready.rpc_id(1, room.room_name, value)

func is_connected_to_server() -> bool:
	return is_instance_valid(multiplayer.multiplayer_peer) and not multiplayer.multiplayer_peer is OfflineMultiplayerPeer and \
		multiplayer.multiplayer_peer.get_connection_status() == MultiplayerPeer.CONNECTION_CONNECTED
# ------------------------------------------------------------------
# |                            PRIVATE                             |
# ------------------------------------------------------------------

func init_client(is_server_: bool, server_: LobbyServer):
	is_server = is_server_
	server = server_
	

func connect_to_server(config: LobbyConfig):
	disconnect_to_server()
	active = Connection.CONNECTING
	
	if config.client_protocol == LobbyConfig.Protocol.ENET:
		client_socket = ENetMultiplayerPeer.new()
		
		if client_socket.create_client(config.server_address, config.server_port) != OK:
			push_error("Cannot connect to server")
			client_socket = null
			return
			
		if config.use_dtls:
			var tls_options := get_tls_options(config)
			var setup_result: Error = client_socket.host.dtls_client_setup(config.server_address, tls_options)
			
			for i in 100:
				if setup_result == OK:
					active = Connection.CONNECTED
					break
				elif i == 1:
					push_error("Cannot setup DTLS")
					active = Connection.DISCONNECTED
					return
				
				setup_result = client_socket.host.dtls_client_setup(config.server_address, tls_options)
	elif config.client_protocol == LobbyConfig.Protocol.WEB_SOCKET:
		var url = config.server_address
		client_socket = WebSocketMultiplayerPeer.new()
		
		if client_socket.create_client(url, get_tls_options(config)) != OK:
			push_error("Cannot connect to server")
			return
			
	else:
		push_error("Protocol not supported")
		return
		
	#client_socket.compress(ENetConnection.COMPRESS_RANGE_CODER)
	multiplayer.set_multiplayer_peer(client_socket)
	
	if is_instance_valid(client_socket) and not client_socket is OfflineMultiplayerPeer:
		multiplayer.peer_connected.connect(func(x):
			if x == 1:
				self.connection_status.emit(ConnectionStatus.CONNECTED)
		)

func disconnect_to_server():
	active = Connection.DISCONNECTED
	multiplayer.multiplayer_peer = null
	if client_socket != null:
		client_socket.close()

func entered_room():
	current_player_info.id = client_socket.get_unique_id()
	current_player_info.ready = false
	server.join_room.rpc_id(1, room.room_name, current_player_info)
	
func _exit_tree():
	if is_instance_valid(client_socket):
		client_socket.disconnect_peer(1, true)
		client_socket.close()

func get_tls_options(config: LobbyConfig) -> TLSOptions:
	if not config.use_dtls:
		return null
		
	if not ResourceLoader.exists(config.certificate_file):
		push_error("Invalid Certificate Path")
		get_tree().quit()
		return
	
	var cert = load(config.certificate_file)
	return TLSOptions.client(cert) if config.validate_certificate else TLSOptions.client_unsafe(cert)

# ------------------------------------------------------------------
# |                              RPCS                              |
# ------------------------------------------------------------------

signal room_updated(room: LobbyRoom)
signal game_started(room: LobbyRoom)

@rpc("any_peer", "call_remote", "reliable")
func update_room(room_name: String, player_infos: Dictionary):
	room = LobbyRoom.new(room_name, player_infos, multiplayer.get_unique_id())
	room_updated.emit(room)

@rpc("any_peer", "call_local", "reliable")
func start_game(player_infos: Dictionary, room_seed):
	room.player_infos = player_infos
	room.room_seed = room_seed
	
	var ids = room.player_infos.keys()
	
	for peer_id in multiplayer.get_peers():
		if not ids.has(peer_id) and peer_id != 1:
			multiplayer.multiplayer_peer.disconnect_peer(peer_id)
	
	game_started.emit(room)

# ------------------------------------------------------------------
